Short Guide for the Mindmaps

The Mindmaps will be updated regulary until the first exam.

psysoc: Social Psychology

psycom: Communication Psychology

psymed: Media Psychology

psycog: Cognitive Psychology


PsyXXX L01-13.emmx will contain all Lectures and an Overview, but in an shortened version.

PsyXXX Lxx - Title.emmx will contain the corresponding lecture. (Maybe some shortenings but not many).

In many Mindmaps there are pictures instead of text. Some of those Pictures will be replaced by text in the future. Some will stay because it is not possible to display them in the Mindmap appropriately or because changing them into text is not necessary or too much work.

Definitions and Classic Studies are surrounded by a parallelogram and marked with either a lightbulb or a loupe.

If you notice spelling mistakes or other mistakes, feel free to leave an issue.
If you leave an issue pleace specify in which file you have found it and where and what the issue is.

I hope the mindmaps will help you prepare for the exam. Good Luck.
